package pl.weatherapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/weather")
public class WeatherController {


    @GetMapping
    public String weather() {
        return "weather";
    }

}
