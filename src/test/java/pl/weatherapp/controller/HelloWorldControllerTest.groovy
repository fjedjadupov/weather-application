package pl.weatherapp.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = HelloWorldController.class)
class HelloWorldControllerTest extends Specification {

    @Autowired (required = false)
    private HelloWorldController helloWorldController

    def "should load HelloWorldController when context arises"() {
        expect: "HelloWorldController is created"
        helloWorldController
    }

    def "should return hello world"() {
        def result = helloWorldController.helloWorld()
        expect: result == "Hello World"
    }
}
